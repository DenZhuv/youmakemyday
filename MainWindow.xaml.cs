﻿using System.Windows;

namespace YouMakeMyDay
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainViewModel _vm;


        public MainWindow()
        {
            InitializeComponent();

            if (DataContext is MainViewModel vm)
            {
                _vm = vm;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            _vm?.GenrateManuallyCreated();
        }
        
        private void ReadExcelButton_Click(object sender, RoutedEventArgs e)
        {
            _vm?.GenerateFromExcel();

        }

        private void ChangeCardTemplateButton_Click(object sender, RoutedEventArgs e)
        {
            _vm?.UpdateCardTemplate();
        }

        private void OpenCardsFolderButton_Click(object sender, RoutedEventArgs e)
        {
            _vm.OpenCardsFolder();
        }
    }
}
