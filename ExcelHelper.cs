﻿using ExcelDataReader;

using System.Collections.Generic;
using System.IO;

namespace YouMakeMyDay
{
    public class ExcelHelper
    {
        private const int RecipientColumnNameIndex = 5;
        private const int MessageColumnNameIndex = 6;
        private const int SenderColumnIndex = 7;

        public IList<CardInfo> GetCardInfoFromExel(string filePath)
        {
            var result = new List<CardInfo>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    bool isHeadersRow = true;
                    do
                    {
                        while (reader.Read())
                        {
                            if (isHeadersRow)
                            {
                                isHeadersRow = false;
                                continue;
                                
                            }
                            result.Add(new CardInfo()
                            {
                                Recipient = reader.GetString(RecipientColumnNameIndex) ?? string.Empty,
                                Message = reader.GetString(MessageColumnNameIndex) ?? string.Empty,
                                Sender = reader.GetString(SenderColumnIndex) ?? string.Empty
                            });
                        }
                    } while (reader.NextResult());
                }
            }

            return result;
        }
    }
}
