﻿using System.Drawing;
using System.Drawing.Imaging;

namespace YouMakeMyDay
{
    public static class CardGenerator
    {
        private const int DefaultFontSize = 80;

        private const int StartX = 80;
        private const int StartRecipientY = 390;        
        private const int StartMessageY = 590;
        private const int StartSenderY = 1020;

        private const int EndX = 1580;
        private const int MaxRecipientHeight = 120;
        private const int MaxMessageHeight = 370;
        private const int MaxSenderHeight = 120;
        
        private const int MaxTextWidth = EndX - StartX;
        private const int MaxSenderWidth = MaxTextWidth - 220;

        public static void DrawCard(string recipient, string message, string sender, string cardTemplatePath)
        {
            using (var img = Image.FromFile(cardTemplatePath))
            {
                using (var drawing = Graphics.FromImage(img))
                {
                    using (Brush textBrush = new SolidBrush(Color.Black))
                    {
                        drawing.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                        drawing.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

                        DrawString(drawing, textBrush, new RectangleF(StartX, StartRecipientY, MaxTextWidth, MaxRecipientHeight), recipient);
                        DrawString(drawing, textBrush, new RectangleF(StartX, StartMessageY, MaxTextWidth, MaxMessageHeight), message, true);
                        DrawString(drawing, textBrush, new RectangleF(StartX, StartSenderY, MaxSenderWidth, MaxSenderHeight), sender);
                        
                        drawing.Save();
                    }
                }

                string savePath = FilesHelper.GetPathForSaveFile();
                img.Save(savePath, ImageFormat.Png);
            }
        }

        private static void DrawString(Graphics drawing, Brush textBrush, RectangleF area, string text, bool isMultiline = false)
        {
            int fontSize = DefaultFontSize;
            var fontFamily = new FontFamily("Arial");
            var font = new Font(fontFamily, fontSize);
            try
            {
                while (!CheckCanFitText(text, font, area.Size, isMultiline) && fontSize > 10)
                {
                    font.Dispose();
                    fontSize -= (fontSize > 40 ? 2 : 1);
                    font = new Font(fontFamily, fontSize);
                }

                drawing.DrawString(text, font, textBrush, area);
            }
            finally
            {
                fontFamily.Dispose();
                font.Dispose();
            }
        }

        private static bool CheckCanFitText(string text, Font font, SizeF layoutArea, bool isMultiline)
        {
            Image img = new Bitmap(1, 1);
            var drawing = Graphics.FromImage(img);

            try
            {
                SizeF textSize = drawing.MeasureString(text, font, layoutArea, StringFormat.GenericDefault, out int charactersFitted, out int linesFilled);

                if (charactersFitted < text.Length || (linesFilled > 1 && !isMultiline) || (linesFilled * font.Height > textSize.Height))
                {
                    return false;
                }

                return true;
            }
            finally
            {
                img.Dispose();
                drawing.Dispose();
            }
        }

        
    }
}
