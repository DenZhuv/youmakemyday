﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace YouMakeMyDay
{
    public class MainViewModel : NotifyPropertyChanged
    {

        private string _cardTemplateImagePath;

        public string CardTemplateImagePath
        {
            get => _cardTemplateImagePath;
            set
            {
                _cardTemplateImagePath = value;
                OnPropertyChanged(nameof(CardTemplateImagePath));
            }
        }

        public CardInfo CurrentCardInfo { get; } = new CardInfo();

        public MainViewModel()
        {
            UpdateCardTemplate();
        }

        public void GenerateFromExcel()
        {

            string filePath = FilesHelper.GetExcelFilePath();
            if (string.IsNullOrEmpty(filePath))
                return;

            var reader = new ExcelHelper();
            IEnumerable<CardInfo> cards = reader.GetCardInfoFromExel(filePath);

            try
            {
                foreach (var card in cards)
                {
                    CardGenerator.DrawCard(card.Recipient, card.Message, card.Sender, CardTemplateImagePath);
                    UpdateCardTemplate();
                }
                MessageBox.Show($"Успешно сохранено карточек: {cards.Count()}!");
            }
            catch
            {
                MessageBox.Show($"Что-то пошло не так при сохранении =(");
            }
        }

        public void GenrateManuallyCreated()
        {
            try
            {
                Generate(CurrentCardInfo.Recipient, CurrentCardInfo.Message, CurrentCardInfo.Sender);
                MessageBox.Show("Успешно сохранено!");
                CurrentCardInfo.Clear();
                UpdateCardTemplate();
            }
            catch
            {
                MessageBox.Show($"Что-то пошло не так при сохранении =(");
            }
        }

        public void UpdateCardTemplate()
        {
            CardTemplateImagePath = FilesHelper.GetRandomCardTemplate();
        }
        public void OpenCardsFolder() => FilesHelper.OpenCardsFolder();

        private void Generate(string recipient, string message, string sender)
        {
            CardGenerator.DrawCard(recipient, message, sender, CardTemplateImagePath);
        }
        
    }
}
