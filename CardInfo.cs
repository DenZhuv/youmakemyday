﻿namespace YouMakeMyDay
{
    public class CardInfo : NotifyPropertyChanged
    {
        private string _recipient = string.Empty;
        private string _message = string.Empty;
        private string _sender = string.Empty;

        public string Recipient
        {
            get => _recipient;
            set
            {
                _recipient = value;
                OnPropertyChanged(nameof(Recipient));
            }
        }
        public string Message
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }
        public string Sender
        {
            get => _sender;
            set
            {
                _sender = value;
                OnPropertyChanged(nameof(Sender));
            }
        }

        public void Clear()
        {
            Recipient = string.Empty;
            Message = string.Empty;
            Sender = string.Empty;
        }
    }
}
