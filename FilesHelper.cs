﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace YouMakeMyDay
{
    public static class FilesHelper
    {
        private const string TemplatesFolder = "CardTemplates";

        private static Random _randoizer;

        public static string GetPathForSaveFile()
        {
            string saveDirectoryRoot = GetSaveDirectoryRoot();

            string saveDirectory = Path.Combine(saveDirectoryRoot, DateTime.Today.ToString("dd-MMMM-yy"));
            if (!Directory.Exists(saveDirectory))
            {
                Directory.CreateDirectory(saveDirectory);
            }

            return Path.Combine(saveDirectory, $"{ DateTime.Now:ddMMyy-HHmmss-ffff}.png");
        }

        public static string GetRandomCardTemplate()
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), TemplatesFolder);
            if (!Directory.Exists(path))
            {
                MessageBox.Show($"Поместите шаблоны в папку '{TemplatesFolder}' в папке с программой!");
                Directory.CreateDirectory(path);
                return string.Empty;
            }
            IEnumerable<string> cardTemplates = Directory.GetFiles(path);

            if (!cardTemplates.Any())
            {
                return string.Empty;
            }
            if (_randoizer == null)
            {
                _randoizer = new Random(DateTime.Now.Millisecond);
            }
            return Path.Combine(path, cardTemplates.ElementAt(_randoizer.Next(0, cardTemplates.Count())));
        }

        public static string GetExcelFilePath()
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Title = "Выберите excel файл откуда брать данные";
                dialog.Filter = "Excel Files| *.xls; *.xlsx; *.xlsm";
                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    return string.Empty;
                }
                return dialog.FileName;
            }
        }

        public static void OpenCardsFolder()
        {
            string path = GetSaveDirectoryRoot();
            Process.Start(path, "");
        }

        private static string GetSaveDirectoryRoot()
        {
            string saveDirectoryRoot = Path.Combine(Directory.GetCurrentDirectory(), "Cards");
            if (!Directory.Exists(saveDirectoryRoot))
            {
                Directory.CreateDirectory(saveDirectoryRoot);
            }
            return saveDirectoryRoot;
        }
    }
}
